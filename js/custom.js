var isset = function(arg,callback){
    var result = false;
    if(typeof arg != 'undefined' && arg != '' && arg != 0 && arg != NaN && arg !== false) result = true;
    return (result && typeof callback == 'function')?callback(arg):result;
}

var log = function(output){
    try{
        console.log(output);
    }
    catch(e){
        alert(output);
    }
}

var isArray = function(data){
    var rs = false;

    try{
        rs = (isset(data) && Array.isArray(data)) ? true : false;
    }
    catch(e){
        if(isset(data) && typeof data == 'object'){
            var dataJson = JSON.stringify(data);
            rs = (dataJson.charAt(0) == '[') ? true : false;
        }
    }

    return rs;
}

var isObject = function(data){
    return (isset(data) && !isArray(data) && typeof data == 'object') ? true : false;
}

var attrAttach = function(asignData,method){
    var thumbReload = false;

    if(isset(asignData,function(data){return(typeof data == 'object')?true:false}) && asignData.length > 0){
        $.each(asignData,function(key,val){
            switch(true){
                case (method == 'class'):
                    $(val[1]).addClass(val[0]);
                break;
                case (method.indexOf('data',0)):
                    $(val[1]).data(method,val[0]);
                break;
                default:
                    $(val[1]).attr(method,val[0]);
                break;
            }
        });
    }
}

$(function(){

    /*
    var addClass = []

    attrAttach(addClass,'class');
    */


    /* 轉移內頁抬頭至位置 */
    /*$('#colMain h2.contentTitle').appendTo('#mainTitle');*/


    /* 設定 banner 圖片成背景 */
    /*
    $('#banner .owl-carousel .owl-item').each(function(){
        var itemThis = $(this);
        var setBgImg = function(){
            var imgPath = itemThis.find('img').attr('src');
            itemThis.css({ 'background-image':'url("'+ imgPath +'")' });
        }

        setBgImg(itemThis);
        $(window).resize(function(){
            setBgImg(itemThis);
        });
    });
    */


    /** 修改九宮格寬度 **/
    /*
    var gridWidth = function(){
        var colItem = $('#colMain .list_switch_content #list_grid_row .list_grid_col');
        colItem.removeClass('col-xs-12 col-md-4 col-sm-6');
        colItem.addClass('col-xs-12 col-sm-6 col-md-4 col-lg-3');
    }

    gridWidth();

    $('.switch-btn[data-switch="grid"]').click(function(){
        gridWidth();
    });
    */


    /* 強制產品詳細頁表格加上響應式外框 */
    $('main #colMain #content_tab table,main #colMain #specification_tab table,main #colMain #features_tab table').each(function(){
        if($(this).parent('.table-responsive').length <= 0){
            $(this).wrap('<div class="table-responsive"></div>');
        }
    });

});